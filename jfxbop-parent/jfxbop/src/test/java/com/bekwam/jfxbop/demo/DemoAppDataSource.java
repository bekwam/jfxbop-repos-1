package com.bekwam.jfxbop.demo;

import com.bekwam.jfxbop.data.BaseManagedDataSource;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Demonstrates the usage of a ManagedDataSource
 *
 * @author carl_000
 * @since 1.0.0
 */
public class DemoAppDataSource extends BaseManagedDataSource {

    private final static Logger logger = LoggerFactory.getLogger(DemoAppDataSource.class);

    /***
     * Property that tracks initialization of object
     *
     * Used in ManagedDataSource framework
     */
    private Boolean initialized = false;

    /**
     * A "business property" representing a link to back-end services and data*
     */
    private final IntegerProperty numUpdates = new SimpleIntegerProperty(-1);

    /**
     * Called by the framework (a Google Guice AOP Interceptor)
     *
     * @throws Exception
     */
    @Override
    public void init() throws Exception {
        super.init();

        if( logger.isDebugEnabled() ) {
            logger.debug("[INIT] initalizing ds");
        }

        numUpdates.set(0);
        initialized = true;

        if( initCB != null ) {
            initCB.accept(null);
        }
    }

    @Override
    public boolean isInitialized() {
        if( logger.isDebugEnabled() ) {
            logger.debug("[IS INIT] initialized={}", initialized);
        }
        return initialized;
    }

    public IntegerProperty getNumUpdates() {
        if( logger.isDebugEnabled() ) {
            logger.debug("[GET # UPDATES] num={}", numUpdates.get());
        }
        return numUpdates;
    }

    public void incrNumUpdates() {
        if( logger.isDebugEnabled() ) {
            logger.debug("[INCR # UPDATES] num was={}", numUpdates.get());
        }
        numUpdates.set( numUpdates.get() + 1 );

        if( logger.isDebugEnabled() ) {
            logger.debug("[INCR # UPDATES] num now={}", numUpdates.get());
        }
    }
}
