package com.bekwam.jfxbop.demo;

import com.bekwam.jfxbop.data.ManagedDataSource;
import com.bekwam.jfxbop.data.ManagedDataSourceInterceptor;
import com.google.inject.AbstractModule;
import com.google.inject.matcher.Matchers;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.util.BuilderFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Guice AbstractModule for DemoApp
 *
 * @author carl_000
 * @since 1.0.0
 */
public class DemoAppGuiceModule extends AbstractModule {

    private final static Logger logger = LoggerFactory.getLogger(DemoAppGuiceModule.class);

    @Override
    protected void configure() {

        if( logger.isDebugEnabled() ) {
            logger.debug("[CONFIGURE] configuring");
        }

        bind(BuilderFactory.class).to(JavaFXBuilderFactory.class);

        bindInterceptor(Matchers.subclassesOf(ManagedDataSource.class), Matchers.any(), new ManagedDataSourceInterceptor());
    }
}
