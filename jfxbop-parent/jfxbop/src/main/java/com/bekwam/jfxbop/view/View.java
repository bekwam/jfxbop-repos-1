package com.bekwam.jfxbop.view;

/**
 * Fundamental interface for a UI screen
 *
 * @author carl_000
 * @since 1.0.0
 */
public interface View {

    /**
     * Shows the view
     *
     * @throws Exception
     *
     * @since 1.0.0
     */
    void show() throws Exception;

    /**
     * Hides the view
     *
     * @throws Exception
     *
     * @since 1.0.0
     */
    void hide() throws Exception;
}
