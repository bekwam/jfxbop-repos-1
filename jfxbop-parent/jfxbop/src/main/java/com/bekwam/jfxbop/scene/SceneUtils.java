package com.bekwam.jfxbop.scene;

import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * Useful methods for working with JavaFX Scenes
 *
 * @author carl_000
 * @since 1.0.0
 */
public class SceneUtils {

    private Logger logger = LoggerFactory.getLogger(SceneUtils.class);

    /**
     * Removes the stylesheets from the Nodes belonging to the Scene graph
     *
     * @param scene
     *
     * @since 1.0.0
     */
    public void stripStylesheets(Scene scene) {
        forChildrenDo( scene, n -> ((Parent)n).getStylesheets().clear(), n -> n instanceof Parent);
    }

    /**
     * Executes the specified action on each Node in the Scene graph that meet the criteria
     *
     * @param scene
     * @param action
     * @param criteria
     *
     * @since 1.0.0
     */
    public void forChildrenDo(Scene scene, Consumer<Node> action, Predicate<Node> criteria) {
        Parent root = scene.getRoot();
        doForChildrenDo(root, action, criteria);
    }

    /**
     * Executes the specified action on each Node in the Scene graphic (unconditionally)
     *
     * @param scene
     * @param action
     *
     * @since 1.0.0
     */
    public void forAllChildrenDo(Scene scene, Consumer<Node> action) {
        forChildrenDo(scene, action, n -> true );
    }

    protected void doForChildrenDo(Node n, Consumer<Node> action, Predicate<Node> criteria) {

        if( criteria.test(n) ) {
            action.accept(n);
        }

        if( !(n instanceof Parent) ) {
            return;
        }

        Parent p = (Parent)n;

        for( Node child : p.getChildrenUnmodifiable() ) {
            doForChildrenDo(child, action, criteria);
        }
    }
}
