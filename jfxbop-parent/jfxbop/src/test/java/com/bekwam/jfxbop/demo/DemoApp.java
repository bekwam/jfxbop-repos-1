package com.bekwam.jfxbop.demo;

import com.google.inject.Guice;
import com.google.inject.Injector;
import javafx.application.Application;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A demonstration of an App using a ManagedDataSource
 *
 * @author carl_000
 * @since 1.0.0
 */
public class DemoApp extends Application {

    private final static Logger logger = LoggerFactory.getLogger(DemoApp.class);

    @Override
    public void start(Stage primaryStage) throws Exception {

        if( logger.isDebugEnabled() ) {
            logger.debug("[START] starting app");
        }

        //
        // Initiaize Google Guice
        //
        Injector injector = Guice.createInjector(new DemoAppGuiceModule());

        DemoAppController dac = injector.getInstance(DemoAppController.class);

        try {
            dac.show();  // ignoring the primaryStage
        } catch(Exception exc) {
            String msg = "Error launching demo app";
            logger.error( msg, exc );
            Alert alert = new Alert(Alert.AlertType.ERROR, msg);
            alert.showAndWait();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
