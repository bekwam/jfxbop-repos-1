package com.bekwam.jfxbop.demo;

import com.bekwam.jfxbop.guice.GuiceBaseView;
import com.bekwam.jfxbop.view.Viewable;
import com.google.inject.Inject;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.util.converter.NumberStringConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * JavaFX Controller for main DemoApp screen
 *
 * Also a JFXBop Viewable
 *
 * @author carl_000
 * @since 1.0.0
 */
@Viewable(
        fxml="/DemoApp.fxml",
        stylesheet = "/DemoApp.css",
        title = "DemoApp - Num Refreshes"
)
public class DemoAppController extends GuiceBaseView {

    private final static Logger logger = LoggerFactory.getLogger(DemoAppController.class);

    @FXML
    TextField tfNumUpdates;

    @Inject
    DemoAppDataSource ds;

    @FXML
    public void initialize() {

        if( logger.isDebugEnabled() ) {
            logger.debug("[INIT] initializing");
        }

        tfNumUpdates.textProperty().bindBidirectional(ds.getNumUpdates(), new NumberStringConverter());
    }

    @FXML
    public void update() {

        if( logger.isDebugEnabled() ) {
            logger.debug("[UPDATE] updating");
        }

        ds.incrNumUpdates();
    }

}
